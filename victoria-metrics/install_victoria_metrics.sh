#!/usr/bin/bash

###############################################################
#  TITRE: install vm/grafana
#
#  AUTEUR:   Xavier
#  VERSION: 1.0
#  CREATION:  13/06/2023
#
#  DESCRIPTION: 
###############################################################

set -euo pipefail

# Variables ###################################################

VERSION=1.91.3
IP=$(hostname -I | awk '{print $2}')

# Functions ###################################################

victoria_metrics_install(){

wget -qq https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v${VERSION}/victoria-metrics-linux-amd64-v${VERSION}.tar.gz
tar xzf victoria-metrics-linux-amd64-v${VERSION}.tar.gz -C /usr/local/bin/
chmod +x /usr/local/bin/victoria-metrics-prod

groupadd --system victoriametrics
useradd -s /sbin/nologin --system -g victoriametrics victoriametrics

mkdir -p /var/lib/victoria-metrics/
chown victoriametrics:victoriametrics /var/lib/victoria-metrics/

}

victoria_metrics_systemd_service(){
echo "
[Unit]
Description=Description=VictoriaMetrics service
After=network.target

[Service]
Type=simple
LimitNOFILE=2097152
User=victoriametrics
Group=victoriametrics
ExecStart=/usr/local/bin/victoria-metrics-prod \
       -storageDataPath=/var/lib/victoria-metrics/ \
       -httpListenAddr=0.0.0.0:8428 \
       -retentionPeriod=1

SyslogIdentifier=victoriametrics
Restart=always

PrivateTmp=yes
ProtectHome=yes
NoNewPrivileges=yes
ProtectSystem=full

[Install]
WantedBy=multi-user.target
" >/etc/systemd/system/victoriametrics.service
}

victoria_metrics_test(){

curl http://localhost:8428/api/v1/query -d 'query={job=~".*"}'

}

victoria_metrics_restart(){
  systemctl restart victoriametrics
  systemctl enable victoriametrics
}


grafana_install(){
wget -q -O - https://packages.grafana.com/gpg.key | apt-key add -
add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
apt-get update -qq >/dev/null
apt-get install -qq -y grafana >/dev/null
}


grafana_edit_configuration()
{
echo "
datasources:
-  access: 'proxy'
   editable: true 
   is_default: true
   name: 'victoriametrics'
   org_id: 1 
   type: 'prometheus' 
   url: 'http://127.0.0.1:8428' 
   version: 1
" > /etc/grafana/provisioning/datasources/all.yml
sudo chmod 644 /etc/grafana/provisioning/datasources/all.yml
}

grafana_dashboard(){
wget https://raw.githubusercontent.com/rfrail3/grafana-dashboards/master/prometheus/node-exporter-full.json -P /var/lib/grafana/

echo "
apiVersion: 1
providers:
- name: 'node-exporter'
  orgId: 1
  folder: ''
  type: file
  disableDeletion: false
  updateIntervalSeconds: 10 
  options:
    path: /var/lib/grafana/node-exporter-full.json
" > /etc/grafana/provisioning/dashboards/dashboard-node-exporter.yml
chown -R root:grafana /etc/grafana/provisioning/dashboards/dashboard-node-exporter.yml

}

grafana_restart(){
systemctl start grafana-server
systemctl enable grafana-server
}

# Let's Go !! #################################################

victoria_metrics_install
victoria_metrics_systemd_service
victoria_metrics_restart
victoria_metrics_test

grafana_install
grafana_edit_configuration
grafana_dashboard
grafana_restart
