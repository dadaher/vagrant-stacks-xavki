#!/usr/bin/bash

###############################################################
#  TITRE: install vm/grafana
#
#  AUTEUR:   Xavier
#  VERSION: 1.0
#  CREATION:  13/06/2023
#
#  DESCRIPTION: 
###############################################################

set -euo pipefail

# Variables ###################################################

VERSION=1.91.3
IP=$(hostname -I | awk '{print $2}')

# Functions ###################################################

victoria_agent_install(){

wget -qq https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v${VERSION}/vmutils-linux-amd64-v${VERSION}.tar.gz
tar xzf vmutils-linux-amd64-v${VERSION}.tar.gz -C /usr/local/bin/
chmod -R +x /usr/local/bin/

groupadd --system victoriagent
useradd -s /sbin/nologin --system -g victoriagent victoriagent

mkdir -p mkdir -p /etc/victoria-agent/conf/
chown -R victoriagent:victoriagent /etc/victoria-agent

}

victoria_agent_systemd_service(){
echo "
[Unit]
Description=Description=VictoriaAgent service
After=network.target

[Service]
Type=simple
#User=victoriagent
#Group=victoriagent
ExecStart=/usr/local/bin/vmagent-prod \
       -promscrape.config=/etc/victoria-agent/conf/victoria-agent.yml \
       -remoteWrite.url=http://vic1:8428/api/v1/write \
       -promscrape.config.strictParse=false

SyslogIdentifier=victoriagent
Restart=always

[Install]
WantedBy=multi-user.target
" >/etc/systemd/system/victoriagent.service
}


victoria_agent_configuration(){
echo "
global:
  scrape_interval:     5s 
  evaluation_interval: 5s 
  external_labels:
    monitor: 'codelab-monitor'
rule_files:
scrape_configs:
  - job_name: node_exporter
    static_configs:
      - targets: 
" > /etc/victoria-agent/conf/victoria-agent.yml

awk '$1 ~ "^192.168" {print "        - "$2":9100"}' /etc/hosts >> /etc/victoria-agent/conf/victoria-agent.yml

}


victoria_agent_restart(){
  systemctl restart victoriagent
  systemctl enable victoriagent
}


# Let's Go !! #################################################


victoria_agent_install
victoria_agent_systemd_service
victoria_agent_configuration
victoria_agent_restart

